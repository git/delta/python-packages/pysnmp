
TRAP/INFORM notification
========================

.. toctree::
   :maxdepth: 2

.. autofunction:: pysnmp.entity.rfc3413.oneliner.ntforg.sendNotification
