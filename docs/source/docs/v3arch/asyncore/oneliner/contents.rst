
High-level SNMP
===============

There are a handful of most basic SNMP Applications defined by RFC3413 and
called Standard Applications. Those implementing Manager side of the system
(:RFC:`3411#section-3.1.3.1`) are Command Generator (initiating GET, SET,
GETNEXT, GETBULK operations) and Notification Receiver (handling arrived 
notifications). On Agent side (:RFC:`3411#section-3.1.3.2`) there are
Command Responder (handling GET, SET, GETNEXT, GETBULK operations) and
Notification Originator (issuing TRAP and INFORM notifications). In 
PySNMP Standard Applications are implemented on top of SNMPv3 framework.

There're two kinds of high-level programming interfaces to Standard SNMP
Applications: synchronous and asynchronous. They are similar in terms of
call signatures but differ in behaviour. Synchronous calls block the whole
application till requested operation is finished. Asynchronous interface
breaks its synchronous version apart - at first required data are prepared
and put on the outgoing queue. The the application is free to deal with
other tasks till pending message is sent out (by I/O dispacher) and
response is arrived. At that point a previously supplied callback function
will be invoked and response data will be passed along.

Synchronous Command Generator

.. toctree::
   :maxdepth: 2

   /docs/v3arch/asyncore/oneliner/manager/cmdgen/getcmd
   /docs/v3arch/asyncore/oneliner/manager/cmdgen/setcmd
   /docs/v3arch/asyncore/oneliner/manager/cmdgen/nextcmd
   /docs/v3arch/asyncore/oneliner/manager/cmdgen/bulkcmd

Synchronous Notification Originator

.. toctree::
   :maxdepth: 2

   /docs/v3arch/asyncore/oneliner/agent/ntforg/notification 

The asynchronous version is best suited for massively parallel SNMP
messaging possibly handling other I/O activities in the same time. The
synchronous version is advised to employ for singular and blocking
operations as well as for rapid prototyping.

Asynchronous operations
-----------------------

.. toctree::
   :maxdepth: 2

   /docs/v3arch/asyncore/oneliner/manager/cmdgen/async-command-generator
   /docs/v3arch/asyncore/oneliner/agent/ntforg/async-notification-originator

SNMP Configuration
------------------

SNMP security configuration is conveyed to SNMP engine via
:py:class:`~pysnmp.entity.rfc3413.oneliner.auth.CommunityData`
and :py:class:`~pysnmp.entity.rfc3413.oneliner.auth.UsmUserData`
classes:

.. toctree::
   :maxdepth: 2

   /docs/v3arch/asyncore/oneliner/security-configuration

Type of network transport SNMP engine uses along with transport
options is summarized by 
:py:class:`~pysnmp.entity.rfc3413.oneliner.target.UdpTransportTarget`
and
:py:class:`~pysnmp.entity.rfc3413.oneliner.target.Udp6TransportTarget`
container classes:

.. toctree::
   :maxdepth: 2

   /docs/v3arch/asyncore/oneliner/transport-configuration

SNMP engine may serve several instances of the same MIB within
possibly multiple SNMP entities. SNMP context is a method to
unambiguously identify a collection of MIB variables behind
SNMP engine. 
See :RFC:`3411#section-3.3.1` for details.

.. toctree::
   :maxdepth: 2

   /docs/v3arch/asyncore/oneliner/snmp-context

MIB services
------------

MIB variables represent a collection of managed objects,
residing in MIBs. Command Generator applications refer
to MIB variables and their values using
:py:class:`~pysnmp.smi.rfc1902.ObjectType` and
:py:class:`~pysnmp.smi.rfc1902.ObjectIdentity` classes.

.. toctree::
   :maxdepth: 2

   /docs/smi/mib-variables

SNMP Notifications are enumerated and imply including certain
set of MIB variables.
Notification Originator applications refer to MIBs for MIB notifications
that are represented by 
:py:class:`~pysnmp.smi.rfc1902.NotificationType` class instances.

.. toctree::
   :maxdepth: 2

   /docs/smi/mib-notification-types

