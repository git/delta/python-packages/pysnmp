
Security Parameters
===================

Calls to oneliner Applications API consume Security Parameters
configuration object on input. The shortcut classes described in
this section convey configuration information to SNMP engine's
Local Configuration Datastore (:RFC:`2271#section-3.4.2`).
Once committed to LCD, SNMP engine saves its configuration for
the lifetime of SNMP engine object.

Security Parameters object is Security Model specific. 
:py:class:`~pysnmp.entity.rfc3413.oneliner.auth.UsmUserData` class 
serves SNMPv3 User-Based Security Model configuration, while
:py:class:`~pysnmp.entity.rfc3413.oneliner.auth.CommunityData`
class is used for Community-Based Security Model of SNMPv1/SNMPv2c.

.. toctree::
   :maxdepth: 2

.. autoclass:: pysnmp.entity.rfc3413.oneliner.auth.CommunityData(communityIndex, communityName=None, mpModel=1, contextEngineId=None, contextName='', tag='')

.. autoclass:: pysnmp.entity.rfc3413.oneliner.auth.UsmUserData(userName, authKey=None, privKey=None, authProtocol=usmNoAuthProtocol, privProtocol=usmNoPrivProtocol, securityEngineId=None)

Identification of Authentication and Privacy Protocols is done
via constant OIDs:

.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usmNoAuthProtocol
.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usmHMACMD5AuthProtocol
.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usmHMACSHAAuthProtocol

.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usmNoPrivProtocol
.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usmDESPrivProtocol
.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usm3DESEDEPrivProtocol
.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usmAesCfb128Protocol
.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usmAesCfb192Protocol
.. autodata:: pysnmp.entity.rfc3413.oneliner.auth.usmAesCfb256Protocol


