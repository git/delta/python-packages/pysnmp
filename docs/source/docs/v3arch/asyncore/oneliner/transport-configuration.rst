
Transport configuration
=======================

Calls to oneliner Applications API consume Transport configuration
object on input. The following shortcut classes convey configuration
information to SNMP engine's Local Configuration Datastore
(:RFC:`2271#section-3.4.2`) as well as to underlying socket API. Once committed
to LCD, SNMP engine saves its configuration for the lifetime of SNMP 
engine object.

Transport configuration is specific to Transport domain - 
:py:class:`~pysnmp.entity.rfc3413.oneliner.target.UdpTransportTarget`
class represents remote network endpoint of a UDP-over-IPv4 transport,
whereas 
:py:class:`~pysnmp.entity.rfc3413.oneliner.target.Udp6TransportTarget`
is specific to UDP-over-IPv6 communication.

.. toctree::
   :maxdepth: 2

.. autoclass:: pysnmp.entity.rfc3413.oneliner.target.UdpTransportTarget

.. autoclass:: pysnmp.entity.rfc3413.oneliner.target.Udp6TransportTarget

