
SNMP Engine
===========

SNMP Engine is a central, stateful object used by all SNMP v3
substsems.  Calls to high-level Applications API also consume SNMP
Engine object on input.

.. toctree::
   :maxdepth: 2

.. autoclass:: pysnmp.hlapi.asyncore.SnmpEngine(snmpEngineID=None)

