
MIB Variables
=============

SNMP MIB variable is identified by an OBJECT IDENTIFIER (OID) and is 
accompanied by a value belonging to one of SNMP types (:RFC:`1902#section-2`).
This pair is collectively called a variable-binding in SNMP parlance.

The :py:mod:`~pysnmp.smi.rfc1902` module implements :RFC:`1902#section-2`
MACRO definiitons.

.. toctree::
   :maxdepth: 2

.. autoclass:: pysnmp.smi.rfc1902.ObjectIdentity
   :members:

.. autoclass:: pysnmp.smi.rfc1902.ObjectType
   :members:
