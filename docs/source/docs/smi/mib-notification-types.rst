
MIB notification types
======================

ASN.1 macro definition -- NOTIFICATION-TYPE conveys MIB variables to 
be gathered and reported in SNMP Notification. The
:py:mod:`~pysnmp.smi.rfc1902` module implements :RFC:`1902#section-2`
macro definiitons.

.. toctree::
   :maxdepth: 2

.. autoclass:: pysnmp.smi.rfc1902.NotificationType
   :members:
