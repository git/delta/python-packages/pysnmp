
SNMP with Twisted
=================

There are a handful of most basic SNMP Applications defined by RFC3413 and
called Standard Applications. Those implementing Manager side of the system
(:RFC:`3411#section-3.1.3.1`) are Command Generator (initiating GET, SET,
GETNEXT, GETBULK operations) and Notification Receiver (handling arrived 
notifications). On Agent side (:RFC:`3411#section-3.1.3.2`) there are
Command Responder (handling GET, SET, GETNEXT, GETBULK operations) and
Notification Originator (issuing TRAP and INFORM notifications). In 
PySNMP Standard Applications are implemented on top of SNMPv3 framework.

There're two kinds of high-level programming interfaces to Standard SNMP
Applications: synchronous and asynchronous. They are similar in terms of
call signatures but differ in behaviour. Synchronous calls block the whole
application till requested operation is finished. Asynchronous interface
breaks its synchronous version apart - at first required data are prepared
and put on the outgoing queue. The the application is free to deal with
other tasks till pending message is sent out (by I/O dispacher) and
response is arrived. At that point a previously supplied callback function
will be invoked and response data will be passed along.

Command Generator

.. toctree::
   :maxdepth: 2

   /docs/hlapi/twisted/manager/cmdgen/getcmd
   /docs/hlapi/twisted/manager/cmdgen/setcmd
   /docs/hlapi/twisted/manager/cmdgen/nextcmd
   /docs/hlapi/twisted/manager/cmdgen/bulkcmd

Notification Originator

.. toctree::
   :maxdepth: 2

   /docs/hlapi/twisted/agent/ntforg/notification 

The asynchronous version is best suited for massively parallel SNMP
messaging possibly handling other I/O activities in the same time. The
synchronous version is advised to employ for singular and blocking
operations as well as for rapid prototyping.

Transport configuration
-----------------------

Type of network transport SNMP engine uses along with transport
options is summarized by 
:py:class:`~pysnmp.hlapi.twisted.UdpTransportTarget`
container class:

.. toctree::
   :maxdepth: 2

   /docs/hlapi/twisted/transport-configuration

