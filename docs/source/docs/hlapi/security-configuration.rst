
Security Parameters
===================

Calls to high-level Applications API consume Security Parameters
configuration object on input. The shortcut classes described in
this section convey configuration information to SNMP engine's
Local Configuration Datastore (:RFC:`2271#section-3.4.2`).
Once committed to LCD, SNMP engine saves its configuration for
the lifetime of SNMP engine object.

Security Parameters object is Security Model specific. 
:py:class:`~pysnmp.hlapi.UsmUserData` class 
serves SNMPv3 User-Based Security Model configuration, while
:py:class:`~pysnmp.hlapi.CommunityData`
class is used for Community-Based Security Model of SNMPv1/SNMPv2c.

.. toctree::
   :maxdepth: 2

.. autoclass:: pysnmp.hlapi.CommunityData(communityIndex, communityName=None, mpModel=1, contextEngineId=None, contextName='', tag='')

.. autoclass:: pysnmp.hlapi.UsmUserData(userName, authKey=None, privKey=None, authProtocol=usmNoAuthProtocol, privProtocol=usmNoPrivProtocol, securityEngineId=None)

Identification of Authentication and Privacy Protocols is done
via constant OIDs:

.. autodata:: pysnmp.hlapi.usmNoAuthProtocol
.. autodata:: pysnmp.hlapi.usmHMACMD5AuthProtocol
.. autodata:: pysnmp.hlapi.usmHMACSHAAuthProtocol

.. autodata:: pysnmp.hlapi.usmNoPrivProtocol
.. autodata:: pysnmp.hlapi.usmDESPrivProtocol
.. autodata:: pysnmp.hlapi.usm3DESEDEPrivProtocol
.. autodata:: pysnmp.hlapi.usmAesCfb128Protocol
.. autodata:: pysnmp.hlapi.usmAesCfb192Protocol
.. autodata:: pysnmp.hlapi.usmAesCfb256Protocol


