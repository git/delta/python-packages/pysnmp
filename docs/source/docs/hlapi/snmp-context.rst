
SNMP Context
============

Calls to high-level Applications API consume SNMP Context
configuration object on input. The shortcut class described in
this section convey this MIB instance identification information
to SNMP PDU for further communication to peer SNMP engine
(:RFC:`3411#section-4.1`).

.. note::

   SNMP context is only defined within SNMPv3 framework. For SNMPv1/v2c
   architecture integration :RFC:`2576#section-5.1` introduces
   interoperability aid which is available through
   :py:class:`~pysnmp.hlapi.CommunityData`.


.. toctree::
   :maxdepth: 2

.. autoclass:: pysnmp.hlapi.ContextData

