
SNMP data types
===============

SNMP represents real-world objects it serves along with their
states in form of values. Those values each belong to one
of SNMP types (:RFC:`1902#section-2`) which, in turn, are based
on `ASN.1 <https://en.wikipedia.org/wiki/Abstract_Syntax_Notation_One>`_ 
data description language. PySNMP types are derived from
`Python ASN.1 types <http://pyasn1.sf.net>`_ implementation.

.. toctree::
   :maxdepth: 2

.. _integer32:

Integer32 type
--------------

.. autoclass:: pysnmp.proto.rfc1902.Integer32(initializer)
   :members:

.. _integer:

Integer type
------------

.. autoclass:: pysnmp.proto.rfc1902.Integer(initializer)
   :members:

.. _octetstring:

OctetString type
----------------

.. autoclass:: pysnmp.proto.rfc1902.OctetString(strValue=None, hexValue=None)
   :members:

.. _ipaddress:

IpAddress type
----------------

.. autoclass:: pysnmp.proto.rfc1902.IpAddress(strValue=None, hexValue=None)

ObjectIdentifier type
---------------------

.. autoclass:: pysnmp.proto.rfc1902.ObjectIdentifier(initializer)

Counter32 type
--------------

.. autoclass:: pysnmp.proto.rfc1902.Counter32(initializer)

Gauge32 type
------------

.. autoclass:: pysnmp.proto.rfc1902.Gauge32(initializer)

Unsigned32 type
---------------

.. autoclass:: pysnmp.proto.rfc1902.Unsigned32(initializer)

TimeTicks type
--------------

.. autoclass:: pysnmp.proto.rfc1902.TimeTicks(initializer)

Opaque type
-----------

.. autoclass:: pysnmp.proto.rfc1902.Opaque(initializer)

Counter64 type
--------------

.. autoclass:: pysnmp.proto.rfc1902.Counter64(initializer)

Bits type
---------

.. autoclass:: pysnmp.proto.rfc1902.Bits(initializer)
   :members:
