
Low-level SNMP
==============

In cases where performance is your top priority and you only need to 
work with SNMP v1 and v2c systems and you do not mind writing much 
more code, then there is a low-level API to SNMP v1/v2c PDU and 
PySNMP I/O engine. There's practically no SNMP engine or SMI 
infrastructure involved in the operations of these almost wire-level 
interfaces. Although MIB services can still be used separately.

A packet-level API-based application typically manages both SNMP 
message building/parsing and network communication via one or more 
transports. It's fully up to the application to handle failures on 
message and transport levels.

Command Generator Applications
------------------------------

.. toctree::

   /examples/v1arch/asyncore/manager/cmdgen/fetching-variables
   /examples/v1arch/asyncore/manager/cmdgen/modifying-variables
   /examples/v1arch/asyncore/manager/cmdgen/walking-operations
   /examples/v1arch/asyncore/manager/cmdgen/transport-tweaks

Command Responder Applications
------------------------------

.. toctree::

   /examples/v1arch/asyncore/agent/cmdrsp/agent-side-mib-implementations

Notification Originator Applications
------------------------------------

.. toctree::

   /examples/v1arch/asyncore/agent/ntforg/transport-tweaks

Notification Receiver Applications
----------------------------------

.. toctree::

   /examples/v1arch/asyncore/manager/ntfrcv/transport-tweaks

For more details on PySNMP programming model and interfaces, please 
refer to the documentation


