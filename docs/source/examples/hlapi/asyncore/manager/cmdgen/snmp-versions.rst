.. toctree::
   :maxdepth: 2

Various SNMP versions
----------------------

.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/v1-get.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/v1-get.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/v1-get.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/v2c-get.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/v2c-get.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/v2c-get.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/usm-md5-des.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/usm-md5-des.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/usm-md5-des.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/usm-md5-none.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/usm-md5-none.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/usm-md5-none.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/usm-none-none.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/usm-none-none.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/usm-none-none.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/usm-sha-aes128.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/usm-sha-aes128.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/usm-sha-aes128.py>` script.

See also: :doc:`library reference </docs/contents>`.
