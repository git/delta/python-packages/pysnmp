.. toctree::
   :maxdepth: 2

Advanced topics
---------------

.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/getbulk-limit-number-of-packets.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/getbulk-limit-number-of-packets.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/getbulk-limit-number-of-packets.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/multiple-get-calls.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/multiple-get-calls.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/multiple-get-calls.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/custom-contextengineid.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/custom-contextengineid.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/custom-contextengineid.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/custom-contextengineid-and-contextname.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/custom-contextengineid-and-contextname.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/custom-contextengineid-and-contextname.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/custom-v3-security-name.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/custom-v3-security-name.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/custom-v3-security-name.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/specific-v3-engine-id.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/specific-v3-engine-id.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/specific-v3-engine-id.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/query-agents-from-multuple-threads.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/query-agents-from-multuple-threads.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/query-agents-from-multuple-threads.py>` script.


See also: :doc:`library reference </docs/contents>`.
