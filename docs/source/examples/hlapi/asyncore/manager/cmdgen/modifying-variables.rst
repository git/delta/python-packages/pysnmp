
.. toctree::
   :maxdepth: 2

Modifying values
----------------

.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/coerce-set-value-to-mib-spec.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/coerce-set-value-to-mib-spec.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/coerce-set-value-to-mib-spec.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/set-multiple-scalar-values.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/set-multiple-scalar-values.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/set-multiple-scalar-values.py>` script.


See also: :doc:`library-reference </docs/contents>`.
