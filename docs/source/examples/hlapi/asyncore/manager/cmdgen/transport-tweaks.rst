.. toctree::
   :maxdepth: 2

Transport tweaks
----------------

.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/custom-timeout-and-retries.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/custom-timeout-and-retries.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/custom-timeout-and-retries.py>` script.


.. include:: /../../examples/hlapi/asyncore/manager/cmdgen/fetch-variables-over-ipv6.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/manager/cmdgen/fetch-variables-over-ipv6.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/manager/cmdgen/fetch-variables-over-ipv6.py>` script.


See also: :doc:`library reference </docs/contents>`.
