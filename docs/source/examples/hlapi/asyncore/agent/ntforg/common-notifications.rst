.. toctree::
   :maxdepth: 2

Common notifications
--------------------

.. include:: /../../examples/hlapi/asyncore/agent/ntforg/v2c-trap-via-notification-type.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/agent/ntforg/v2c-trap-via-notification-type.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/agent/ntforg/v2c-trap-via-notification-type.py>` script.


.. include:: /../../examples/hlapi/asyncore/agent/ntforg/v3-inform.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/agent/ntforg/v3-inform.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/agent/ntforg/v3-inform.py>` script.

See also: :doc:`library reference </docs/contents>`.
