.. toctree::
   :maxdepth: 2

Advanced topic
--------------

.. include:: /../../examples/hlapi/asyncore/agent/ntforg/custom-contextname.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/agent/ntforg/custom-contextname.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/agent/ntforg/custom-contextname.py>` script.


.. include:: /../../examples/hlapi/asyncore/agent/ntforg/custom-contextengineid.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/agent/ntforg/custom-contextengineid.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/agent/ntforg/custom-contextengineid.py>` script.

See also: :doc:`library reference </docs/contents>`.
