.. toctree::
   :maxdepth: 2

SNMPv1 TRAP variants
--------------------

.. include:: /../../examples/hlapi/asyncore/agent/ntforg/custom-v1-trap.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/agent/ntforg/custom-v1-trap.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/agent/ntforg/custom-v1-trap.py>` script.


.. include:: /../../examples/hlapi/asyncore/agent/ntforg/default-v1-trap.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/hlapi/asyncore/agent/ntforg/default-v1-trap.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/hlapi/asyncore/agent/ntforg/default-v1-trap.py>` script.


See also: :doc:`library reference </docs/contents>`.
