.. toctree::
   :maxdepth: 2

Various SNMP versions
----------------------

.. include:: /../../examples/v3arch/twisted/manager/cmdgen/v1-get.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/manager/cmdgen/v1-get.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/manager/cmdgen/v1-get.py>` script.


.. include:: /../../examples/v3arch/twisted/manager/cmdgen/send-packet-from-specific-address.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/manager/cmdgen/send-packet-from-specific-address.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/manager/cmdgen/send-packet-from-specific-address.py>` script.


.. include:: /../../examples/v3arch/twisted/manager/cmdgen/usm-sha-none.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/manager/cmdgen/usm-sha-none.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/manager/cmdgen/usm-sha-none.py>` script.


.. include:: /../../examples/v3arch/twisted/manager/cmdgen/usm-sha-aes.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/manager/cmdgen/usm-sha-aes.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/manager/cmdgen/usm-sha-aes.py>` script.


See also: :doc:`library reference </docs/contents>`.
