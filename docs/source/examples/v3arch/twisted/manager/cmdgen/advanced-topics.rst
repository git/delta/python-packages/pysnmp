.. toctree::
   :maxdepth: 2

Advanced topics
---------------

.. include:: /../../examples/v3arch/twisted/manager/cmdgen/custom-contextengineid-and-contextname.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/manager/cmdgen/custom-contextengineid-and-contextname.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/manager/cmdgen/custom-contextengineid-and-contextname.py>` script.


See also: :doc:`library reference </docs/contents>`.
