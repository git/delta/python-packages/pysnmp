.. toctree::
   :maxdepth: 2

SNMP conceptual table operations
--------------------------------

.. include:: /../../examples/v3arch/twisted/manager/cmdgen/pull-subtree.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/manager/cmdgen/pull-subtree.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/manager/cmdgen/pull-subtree.py>` script.


See also: :doc:`library reference </docs/contents>`.
