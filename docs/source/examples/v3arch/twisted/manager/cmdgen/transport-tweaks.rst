.. toctree::
   :maxdepth: 2

Transport tweaks
----------------

.. include:: /../../examples/v3arch/twisted/manager/cmdgen/custom-timeout-and-retries.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/manager/cmdgen/custom-timeout-and-retries.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/manager/cmdgen/custom-timeout-and-retries.py>` script.


.. include:: /../../examples/v3arch/twisted/manager/cmdgen/send-packet-from-specific-address.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/manager/cmdgen/send-packet-from-specific-address.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/manager/cmdgen/send-packet-from-specific-address.py>` script.


See also: :doc:`library reference </docs/contents>`.
