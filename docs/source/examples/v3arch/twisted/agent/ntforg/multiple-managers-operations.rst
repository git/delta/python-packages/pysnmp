.. toctree::
   :maxdepth: 2

Multiple managers operations
----------------------------

.. include:: /../../examples/v3arch/twisted/agent/ntforg/send-inform-to-multiple-managers.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/twisted/agent/ntforg/send-inform-to-multiple-managers.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/twisted/agent/ntforg/send-inform-to-multiple-managers.py>` script.


See also: :doc:`library reference </docs/contents>`.
