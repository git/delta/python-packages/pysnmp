.. toctree::
   :maxdepth: 2

Fetching values
---------------

.. include:: /../../examples/v3arch/trollius/manager/cmdgen/v1-get.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/trollius/manager/cmdgen/v1-get.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/trollius/manager/cmdgen/v1-get.py>` script.


See also: :doc:`library reference </docs/contents>`.
