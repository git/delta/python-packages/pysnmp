.. toctree::
   :maxdepth: 2

Transport tweaks
----------------

.. include:: /../../examples/v3arch/trollius/manager/cmdgen/custom-timeout-and-retries.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/trollius/manager/cmdgen/custom-timeout-and-retries.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/trollius/manager/cmdgen/custom-timeout-and-retries.py>` script.


See also: :doc:`library reference </docs/contents>`.
