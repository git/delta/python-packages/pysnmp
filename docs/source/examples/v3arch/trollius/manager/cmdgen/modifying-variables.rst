.. toctree::
   :maxdepth: 2

Modifying values
----------------

.. include:: /../../examples/v3arch/trollius/manager/cmdgen/set-scalar-value.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/trollius/manager/cmdgen/set-scalar-value.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/trollius/manager/cmdgen/set-scalar-value.py>` script.


See also: :doc:`library reference </docs/contents>`.
