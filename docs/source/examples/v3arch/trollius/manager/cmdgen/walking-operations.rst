.. toctree::
   :maxdepth: 2

Walking operations
------------------

.. include:: /../../examples/v3arch/trollius/manager/cmdgen/v3-getbulk.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/trollius/manager/cmdgen/v3-getbulk.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/trollius/manager/cmdgen/v3-getbulk.py>` script.


See also: :doc:`library reference </docs/contents>`.
