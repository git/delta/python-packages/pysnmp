.. toctree::
   :maxdepth: 2

Evaluating NOTIFICATION-TYPE
----------------------------

.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/v2c-trap-via-notification-type.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/v2c-trap-via-notification-type.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/v2c-trap-via-notification-type.py>` script.


.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/send-notification-with-additional-varbinds.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/send-notification-with-additional-varbinds.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/send-notification-with-additional-varbinds.py>` script.


See also: :doc:`library reference </docs/contents>`.
