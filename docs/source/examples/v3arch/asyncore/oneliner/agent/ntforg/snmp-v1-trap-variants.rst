.. toctree::
   :maxdepth: 2

SNMPv1 TRAP variants
--------------------

.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-v1-trap.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-v1-trap.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-v1-trap.py>` script.


.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/default-v1-trap.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/default-v1-trap.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/default-v1-trap.py>` script.


See also: :doc:`library reference </docs/contents>`.
