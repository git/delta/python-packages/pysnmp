.. toctree::
   :maxdepth: 2

Advanced topic
--------------

.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-contextname.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-contextname.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-contextname.py>` script.


.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-contextengineid.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-contextengineid.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/custom-contextengineid.py>` script.

See also: :doc:`library reference </docs/contents>`.
