.. toctree::
   :maxdepth: 2

Various SNMP versions
----------------------

.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/default-v1-trap.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/default-v1-trap.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/default-v1-trap.py>` script.


.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/v2c-trap-via-notification-type.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/v2c-trap-via-notification-type.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/v2c-trap-via-notification-type.py>` script.


.. include:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/v3-trap.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/agent/ntforg/v3-trap.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/agent/ntforg/v3-trap.py>` script.


See also: :doc:`library reference </docs/contents>`.
