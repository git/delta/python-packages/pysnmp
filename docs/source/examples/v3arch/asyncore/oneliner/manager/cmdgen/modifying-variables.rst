
.. toctree::
   :maxdepth: 2

Modifying values
----------------

.. include:: /../../examples/v3arch/asyncore/oneliner/manager/cmdgen/coerce-set-value-to-mib-spec.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/manager/cmdgen/coerce-set-value-to-mib-spec.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/manager/cmdgen/coerce-set-value-to-mib-spec.py>` script.


.. include:: /../../examples/v3arch/asyncore/oneliner/manager/cmdgen/set-multiple-scalar-values.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncore/oneliner/manager/cmdgen/set-multiple-scalar-values.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncore/oneliner/manager/cmdgen/set-multiple-scalar-values.py>` script.


See also: :doc:`library-reference </docs/contents>`.
