.. toctree::
   :maxdepth: 2

Walking operations
------------------

.. include:: /../../examples/v3arch/asyncio/manager/cmdgen/v2c-getbulk.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncio/manager/cmdgen/v2c-getbulk.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncio/manager/cmdgen/v2c-getbulk.py>` script

.. include:: /../../examples/v3arch/asyncio/manager/cmdgen/send-packet-from-specific-address.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncio/manager/cmdgen/send-packet-from-specific-address.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncio/manager/cmdgen/send-packet-from-specific-address.py>` script


See also: :doc:`library reference </docs/contents>`.
