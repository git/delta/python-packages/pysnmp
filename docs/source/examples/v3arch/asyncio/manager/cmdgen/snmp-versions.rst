.. toctree::
   :maxdepth: 2

Various SNMP versions
----------------------

.. include:: /../../examples/v3arch/asyncio/manager/cmdgen/set-multiple-scalar-values.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncio/manager/cmdgen/set-multiple-scalar-values.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncio/manager/cmdgen/set-multiple-scalar-values.py>` script.

.. include:: /../../examples/v3arch/asyncio/manager/cmdgen/fetch-scalar-value.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncio/manager/cmdgen/fetch-scalar-value.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncio/manager/cmdgen/fetch-scalar-value.py>` script.


See also: :doc:`library reference </docs/contents>`.
