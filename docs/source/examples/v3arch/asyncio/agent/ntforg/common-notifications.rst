.. toctree::
   :maxdepth: 2

Common notifications
--------------------

.. include:: /../../examples/v3arch/asyncio/agent/ntforg/v1-trap.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncio/agent/ntforg/v1-trap.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncio/agent/ntforg/v1-trap.py>` script.

.. include:: /../../examples/v3arch/asyncio/agent/ntforg/v3-inform.py
   :start-after: """
   :end-before: """#

.. literalinclude:: /../../examples/v3arch/asyncio/agent/ntforg/v3-inform.py
   :start-after: """#
   :language: python

:download:`Download</../../examples/v3arch/asyncio/agent/ntforg/v3-inform.py>` script.

See also: :doc:`library reference </docs/contents>`.
