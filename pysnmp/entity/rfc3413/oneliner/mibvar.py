# Obsolete interfaces, use pysnmp.smi.rfc1902 instead.
from pysnmp.smi import rfc1902

class MibVariable(rfc1902.ObjectIdentity): pass
class MibVariableBinding(rfc1902.ObjectType): pass

