"""
SNMP INFORM notification
++++++++++++++++++++++++

Send SNMP INFORM notification using the following options:

* SNMPv3
* with user 'usr-md5-none', auth: MD5, priv NONE
* over IPv4/UDP
* using Trollius framework for network transport
* to a Manager at 127.0.0.1:162
* send INFORM notification
* with TRAP ID 'warmStart' specified as an OID
* include managed object information 1.3.6.1.2.1.1.5.0 = 'system name'

Functionally similar to:

| $ snmpinform -v3 -l authPriv -u usr-md5-none -A authkey1 127.0.0.1 0 1.3.6.1.6.3.1.1.5.1 1.3.6.1.2.1.1.5.0 s 'system name'

Requires Trollius framework!

"""#
from pysnmp.entity import engine, config
from pysnmp.entity.rfc3413 import context
from pysnmp.entity.rfc3413.asyncio import ntforg
from pysnmp.carrier.asyncio.dgram import udp
from pysnmp.proto import rfc1902
import trollius

# Get the event loop for this thread
loop = trollius.get_event_loop()

# Create SNMP engine instance
snmpEngine = engine.SnmpEngine()

# SNMPv3/USM setup

# Add USM user
config.addV3User(
    snmpEngine, 'usr-md5-none',
    config.usmHMACMD5AuthProtocol, 'authkey1'
)
config.addTargetParams(snmpEngine, 'my-creds', 'usr-md5-none', 'authNoPriv')

# Transport setup

#
# Setup transport endpoint and bind it with security settings yielding
# a target name. Since Notifications could be sent to multiple Managers
# at once, more than one target entry may be configured (and tagged).
#
config.addTransport(
    snmpEngine,
    udp.domainName,
    udp.UdpTransport().openClientMode()
)
config.addTargetAddr(
    snmpEngine, 'my-nms',
    udp.domainName, ('127.0.0.1', 162),
    'my-creds',
    tagList='all-my-managers'
)

# Specify what kind of notification should be sent (TRAP or INFORM),
# to what targets (chosen by tag) and what filter should apply to
# the set of targets (selected by tag)
config.addNotificationTarget(
    snmpEngine, 'my-notification', 'my-filter', 'all-my-managers', 'inform'
)

# Allow NOTIFY access to Agent's MIB by this SNMP model (3), securityLevel
# and SecurityName
config.addContext(snmpEngine, '')
config.addVacmUser(snmpEngine, 3, 'usr-md5-none', 'authNoPriv', (), (), (1,3,6))

@trollius.coroutine
def snmpOperation(snmpEngine, target, snmpContext, contextName,
                  notificationName, instanceIndex, additionalVarBinds):
    ( snmpEngine,
      errorIndication,
      errorStatus,
      errorIndex, 
      varBinds ) = yield trollius.From(
        ntforg.NotificationOriginator().sendVarBinds(
          snmpEngine,
          target,
          snmpContext,
          contextName,
          notificationName,
          instanceIndex,
          additionalVarBinds
        )
    )

    print('Notification status - %s' % (
            errorIndication and errorIndication or 'delivered'
        )
    )

    # This also terminates internal timer
    config.delTransport(
        snmpEngine,
        udp.domainName
    ).closeTransport()

# Initiate sending SNMP message
loop.run_until_complete(
    snmpOperation(
        snmpEngine,
        # Notification targets
        'my-notification',
        # Default SNMP context where contextEngineId == SnmpEngineId
        context.SnmpContext(snmpEngine),
        # contextName
        '',
        # notification name (SNMPv2-MIB::coldStart)
        (1,3,6,1,6,3,1,1,5,1),
        # notification objects instance index
        None,
        # additional var-binds: ( (oid, value), ... )
        [ ((1,3,6,1,2,1,1,5,0), rfc1902.OctetString('system name')) ]
    )
)

# Clear the event loop
loop.close()
