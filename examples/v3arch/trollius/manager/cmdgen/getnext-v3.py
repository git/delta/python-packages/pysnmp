"""
Walk MIB
++++++++

Send a series of SNMP GETNEXT requests
* with SNMPv3 with user 'usr-none-none', no auth and no privacy protocols
* using Trollius framework for network transport
* over IPv4/UDP
* to an Agent at 195.218.195.228:161
* for an OID in string form
* stop on end-of-mib condition for both OIDs

This script performs similar to the following Net-SNMP command:

| $ snmpwalk -v2c -c public -ObentU 195.218.195.228 1.3.6.1.2.1.1 1.3.6.1.4.1.1

Requires Trollius framework!

"""#
from pysnmp.entity import engine, config
from pysnmp.proto import rfc1905
from pysnmp.entity.rfc3413.asyncio import cmdgen
from pysnmp.carrier.asyncio.dgram import udp
import trollius

# Get the event loop for this thread
loop = trollius.get_event_loop()

# Create SNMP engine instance
snmpEngine = engine.SnmpEngine()

#
# SNMPv3/USM setup
#

# user: usr-none-none, auth: none, priv: none
config.addV3User(
    snmpEngine, 'usr-none-none',
)
config.addTargetParams(snmpEngine, 'my-creds', 'usr-none-none', 'noAuthNoPriv')

#
# Setup transport endpoint and bind it with security settings yielding
# a target name
#

# UDP/IPv4
config.addTransport(
    snmpEngine,
    udp.domainName,
    udp.UdpTransport().openClientMode()
)
config.addTargetAddr(
    snmpEngine, 'my-router',
    udp.domainName, ('195.218.195.228', 161),
    'my-creds'
)

@trollius.coroutine
def snmpOperation(snmpEngine, target, contextEngineId, contextName, varBinds):
    initialVarBinds = varBinds
    while varBinds:
        ( snmpEngine,
          errorIndication,
          errorStatus,
          errorIndex, 
          varBindTable ) = yield trollius.From(
              cmdgen.NextCommandGenerator().sendVarBinds(
                snmpEngine,
                target,
                contextEngineId,
                contextName,
                varBinds
              )
          )

        if errorIndication:
            print(errorIndication)
            break
        elif errorStatus:
            print('%s at %s' % (
                    errorStatus.prettyPrint(),
                    errorIndex and varBindTable[-1][int(errorIndex)-1][0] or '?'
                )
            )
            break
        else:
            for varBindRow in varBindTable:
                for oid, val in varBindRow:
                    print('%s = %s' % (oid.prettyPrint(), val.prettyPrint()))

            errorIndication, varBinds = cmdgen.getNextVarBinds(
                initialVarBinds, varBindRow
            )
             
    # This also terminates internal timer
    config.delTransport(
        snmpEngine,
        udp.domainName
    ).closeTransport()

    loop.stop()

loop.run_until_complete(
    snmpOperation(
        snmpEngine,
        'my-router',
        None, '',  # contextEngineId, contextName 
        ( ('1.3.6.1.2.1.1', None), ('1.3.6.1.2.1.11', None) )
    )
)

loop.close()
