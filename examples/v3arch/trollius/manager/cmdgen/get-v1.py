"""
SNMPv1 GET
++++++++++

Send SNMP GET request using the following options:

* with SNMPv1, community 'public'
* using Trollius framework for network transport
* over IPv4/UDP
* to an Agent at 195.218.195.228:161
* for an OID in string form

This script performs similar to the following Net-SNMP command:

| $ snmpset -v1 -c public -ObentU 195.218.195.228 1.3.6.1.2.1.1.1.0

Requires Trollius framework!

"""#
from pysnmp.carrier.asyncio.dgram import udp
from pysnmp.entity.rfc3413.asyncio import cmdgen
from pysnmp.entity import engine, config
import trollius

# Get the event loop for this thread
loop = trollius.get_event_loop()

# Create SNMP engine instance
snmpEngine = engine.SnmpEngine()

#
# SNMPv2c setup
#

# SecurityName <-> CommunityName mapping
config.addV1System(snmpEngine, 'my-area', 'public')

# Specify security settings per SecurityName (SNMPv1 - 0, SNMPv2c - 1)
config.addTargetParams(snmpEngine, 'my-creds', 'my-area', 'noAuthNoPriv', 0)

#
# Setup transport endpoint and bind it with security settings yielding
# a target name
#

# UDP/IPv4
config.addTransport(
    snmpEngine,
    udp.domainName,
    udp.UdpTransport().openClientMode()
)
config.addTargetAddr(
    snmpEngine, 'my-router',
    udp.domainName, ('195.218.195.228', 161),
    'my-creds'
)

@trollius.coroutine
def snmpOperation(snmpEngine, target, contextEngineId, contextName, varBinds):
    ( snmpEngine,
      errorIndication,
      errorStatus,
      errorIndex, 
      varBinds ) = yield trollius.From(
          cmdgen.GetCommandGenerator().sendVarBinds(
            snmpEngine,
            target,
            contextEngineId,
            contextName,
            varBinds
        )
    )

    if errorIndication:
        print(errorIndication)
    elif errorStatus:
        print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1][0] or '?'
            )
        )
    else:
        for oid, val in varBinds:
            print('%s = %s' % (oid.prettyPrint(), val.prettyPrint()))

    # This also terminates internal timer
    config.delTransport(
        snmpEngine,
        udp.domainName
    ).closeTransport()

loop.run_until_complete(
    snmpOperation(
        snmpEngine,
        'my-router',
        None, '',  # contextEngineId, contextName
        ( ('1.3.6.1.2.1.1.1.0', None), )
    )
)

loop.close()
