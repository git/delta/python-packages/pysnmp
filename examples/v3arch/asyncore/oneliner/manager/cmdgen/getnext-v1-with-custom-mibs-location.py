"""
Search PySNMP MIBs at custom location
+++++++++++++++++++++++++++++++++++++

Perform SNMP GETNEXT operation and resolve received var-binds with
PySNMP MIB(s) found at a custom location. Use the following SNMP options:

* with SNMPv1, community 'public'
* over IPv4/UDP
* to an Agent at demo.snmplabs.com:161
* two columns of the IF-MIB::ifEntry table
* search for pysnmp MIBs at specific filesystem location
* stop when response OIDs leave the scopes of initial OIDs

The pysnmp engine maintains its own MIB search path which is used
for searching for MIB modules by name. By default pysnmp will search
its built-in MIB repository (pysnmp.smi.mibs.instances, pysnmp.smi.mibs)
and pysnmp-mibs package (pysnmp_mibs). Those two entries are normally
found relative to $PYTHONPATH. Besides searching MIBs as Python modules
(which could be egg-ed or not), pysnmp can also search for stand-alone
files in given directories. The latter could be specified by 
ObjectIdentity.addMibSource() calls. New search path entries are added
in front of existing ones in search path.

Functionally similar to:

| $ snmpwalk -v1 -c public -ObentU demo.snmplabs.com \
|                      IF-MIB::ifDescr IF-MIB::ifType

"""#
from pysnmp.entity.rfc3413.oneliner.cmdgen import *

for errorIndication, \
    errorStatus, errorIndex, \
    varBinds in nextCmd(SnmpEngine(),
                        CommunityData('public', mpModel=0),
                        UdpTransportTarget(('demo.snmplabs.com', 161)),
                        ContextData(),
                        ObjectType(ObjectIdentity('IF-MIB', 'ifDescr').addMibSource('/tmp/mymibs')),
                        ObjectType(ObjectIdentity('IF-MIB', 'ifType')),
                        lexicographicMode=False):

    if errorIndication:
        print(errorIndication)
        break
    elif errorStatus:
        print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1][0] or '?'
            )
        )
        break
    else:
        for varBind in varBinds:
            print(' = '.join([ x.prettyPrint() for x in varBind ]))
