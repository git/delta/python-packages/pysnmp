#
# GETNEXT Command Generator Application
#
# Perform SNMP GETNEXT operation with the following options:
#
# with SNMPv1, community 'public'
# over IPv4/UDP
# to an Agent at demo.snmplabs.com:161
# two columns of the IF-MIB::ifEntry table
# search for pysnmp MIBs at specific filesystem location
# stop when response OIDs leave the scopes of initial OIDs
#
# This script performs similar to the following Net-SNMP command:
#
# $ snmpwalk -v1 -c public -ObentU demo.snmplabs.com IF-MIB::ifDescr IF-MIB::ifType
#
# The pysnmp engine maintains its own MIB search path which is usued
# for searching for MIB modules by name. By default pysnmp will search
# its built-in MIB repository (pysnmp.smi.mibs.instances, pysnmp.smi.mibs)
# and pysnmp-mibs package (pysnmp_mibs). Those two entries are normally
# found relative to $PYTHONPATH. Besides searching MIBs as Python modules
# (which could be egg-ed or not), pysnmp can also search for stand-alone
# files in given directories. The latter could be specified by 
# ObjectIdentity.addMibSource() calls. New search path entries are added
# in front of existing ones in search path.
#
from pysnmp.entity.rfc3413.oneliner import cmdgen

cmdGen = cmdgen.CommandGenerator()

errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.nextCmd(
    cmdgen.CommunityData('public', mpModel=0),
    cmdgen.UdpTransportTarget(('demo.snmplabs.com', 161)),
    cmdgen.ObjectIdentity('IF-MIB', 'ifDescr').addMibSource('/tmp/mymibs'),
    cmdgen.ObjectIdentity('IF-MIB', 'ifType')
)

if errorIndication:
    print(errorIndication)
else:
    if errorStatus:
        print('%s at %s' % (
            errorStatus.prettyPrint(),
            errorIndex and varBindTable[-1][int(errorIndex)-1] or '?'
            )
        )
    else:
        for varBindTableRow in varBindTable:
            for name, val in varBindTableRow:
                print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
