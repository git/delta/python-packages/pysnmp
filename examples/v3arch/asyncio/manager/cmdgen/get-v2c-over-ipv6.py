"""
Fetch scalar value over IPv6
++++++++++++++++++++++++++++

Send SNMP GET request with the following options:

* with SNMPv2c, community 'public'
* using Asyncio framework for network transport
* over IPv6/UDP
* to an Agent at [::]:161
* for an OID in string form

This script performs similar to the following Net-SNMP command:

| $ snmpget -v2c -c public -ObentU udp6:[::1]:161 1.3.6.1.2.1.1.1.0

Requires Python 3.4 and later!

"""#
from pysnmp.carrier.asyncio.dgram import udp6
from pysnmp.entity.rfc3413.asyncio import cmdgen
from pysnmp.entity import engine, config
import asyncio

# Get the event loop for this thread
loop = asyncio.get_event_loop()

# Create SNMP engine instance
snmpEngine = engine.SnmpEngine()

#
# SNMPv2c setup
#

# SecurityName <-> CommunityName mapping
config.addV1System(snmpEngine, 'my-area', 'public')

# Specify security settings per SecurityName (SNMPv1 - 0, SNMPv2c - 1)
config.addTargetParams(snmpEngine, 'my-creds', 'my-area', 'noAuthNoPriv', 1)

#
# Setup transport endpoint and bind it with security settings yielding
# a target name
#

# UDP/IPv6
config.addTransport(
    snmpEngine,
    udp6.domainName,
    udp6.Udp6Transport().openClientMode()
)
config.addTargetAddr(
    snmpEngine, 'my-router',
    udp6.domainName, ('::1', 161),
    'my-creds'
)

@asyncio.coroutine
def snmpOperation(snmpEngine, target, contextEngineId, contextName, varBinds):
    ( snmpEngine,
      errorIndication,
      errorStatus,
      errorIndex, 
      varBinds ) = yield from cmdgen.GetCommandGenerator().sendVarBinds(
        snmpEngine,
        target,
        contextEngineId,
        contextName,
        varBinds
      )

    if errorIndication:
        print(errorIndication)
    elif errorStatus:
        print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1][0] or '?'
            )
        )
    else:
        for oid, val in varBinds:
            print('%s = %s' % (oid.prettyPrint(), val.prettyPrint()))

    # This also terminates internal timer
    config.delTransport(
        snmpEngine,
        udp6.domainName
    ).closeTransport()

loop.run_until_complete(
    snmpOperation(
        snmpEngine,
        'my-router',
        None, '',  # contextEngineId, contextName
        ( ('1.3.6.1.2.1.1.1.0', None), )
    )
)

loop.close()
