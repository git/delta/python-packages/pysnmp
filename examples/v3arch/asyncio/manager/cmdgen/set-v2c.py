"""
SET string and integer scalars
++++++++++++++++++++++++++++++

Send SNMP SET request using the following options:

* with SNMPv1, community 'public'
* using Asyncio framework for network transport
* over IPv4/UDP
* to an Agent at 195.218.195.228:161
* for OIDs in tuple form and an integer and string-typed values

This script performs similar to the following Net-SNMP command:

| $ snmpset -v1 -c private -ObentU 195.218.195.228:161 1.3.6.1.2.1.1.9.1.3.1 s 'my value'  1.3.6.1.2.1.1.9.1.4.1 t 123 

Requires Python 3.4 and later!

"""#
from pysnmp.carrier.asyncio.dgram import udp
from pysnmp.entity.rfc3413.asyncio import cmdgen
from pysnmp.entity import engine, config
from pysnmp.proto import rfc1902
import asyncio

# Get the event loop for this thread
loop = asyncio.get_event_loop()

# Create SNMP engine instance
snmpEngine = engine.SnmpEngine()

#
# SNMPv2c setup
#

# SecurityName <-> CommunityName mapping
config.addV1System(snmpEngine, 'my-area', 'public')

# Specify security settings per SecurityName (SNMPv1 - 0, SNMPv2c - 1)
config.addTargetParams(snmpEngine, 'my-creds', 'my-area', 'noAuthNoPriv', 0)

#
# Setup transport endpoint and bind it with security settings yielding
# a target name
#

# UDP/IPv4
config.addTransport(
    snmpEngine,
    udp.domainName,
    udp.UdpTransport().openClientMode()
)
config.addTargetAddr(
    snmpEngine, 'my-router',
    udp.domainName, ('195.218.195.228', 161),
    'my-creds'
)

@asyncio.coroutine
def snmpOperation(snmpEngine, target, contextEngineId, contextName, varBinds):
    ( snmpEngine,
      errorIndication,
      errorStatus,
      errorIndex, 
      varBinds ) = yield from cmdgen.SetCommandGenerator().sendVarBinds(
            snmpEngine,
            target,
            contextEngineId,
            contextName,
            varBinds
      )

    if errorIndication:
        print(errorIndication)
    elif errorStatus:
        print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1][0] or '?'
            )
        )
    else:
        for oid, val in varBinds:
            print('%s = %s' % (oid.prettyPrint(), val.prettyPrint()))

    # This also terminates internal timer
    config.delTransport(
        snmpEngine,
        udp.domainName
    ).closeTransport()

loop.run_until_complete(
    snmpOperation(
        snmpEngine,
        'my-router',
        None, '',  # contextEngineId, contextName
        [ ((1,3,6,1,2,1,1,9,1,3,1), rfc1902.OctetString('my value')),
          ((1,3,6,1,2,1,1,9,1,4,1), rfc1902.TimeTicks(123)) ]
    )
)

loop.close()
