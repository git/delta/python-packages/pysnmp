"""
Bulk walk MIB
+++++++++++++

Send a series of SNMP GETBULK requests with the following options:

* with SNMPv2c, community 'public'
* using Asyncio framework for network transport
* over IPv4/UDP
* to an Agent at 195.218.195.228:161
* with values non-repeaters = 0, max-repetitions = 25
* for two OIDs in string form
* stop on end-of-mib condition for both OIDs

This script performs similar to the following Net-SNMP command:

| $ snmpbulkwalk -v2c -c public -C n0 -C r25 -ObentU 195.218.195.228 1.3.6.1.2.1.1 1.3.6.1.4.1.1

Requires Python 3.4 and later!

"""#
from pysnmp.entity import engine, config
from pysnmp.proto import rfc1905
from pysnmp.entity.rfc3413.asyncio import cmdgen
from pysnmp.carrier.asyncio.dgram import udp
import asyncio

# Get the event loop for this thread
loop = asyncio.get_event_loop()

# Create SNMP engine instance
snmpEngine = engine.SnmpEngine()

#
# SNMPv2c setup
#

# SecurityName <-> CommunityName mapping
config.addV1System(snmpEngine, 'my-area', 'public')

# Specify security settings per SecurityName (SNMPv1 - 0, SNMPv2c - 1)
config.addTargetParams(snmpEngine, 'my-creds', 'my-area', 'noAuthNoPriv', 1)

#
# Setup transport endpoint and bind it with security settings yielding
# a target name
#

# UDP/IPv4
config.addTransport(
    snmpEngine,
    udp.domainName,
    udp.UdpTransport().openClientMode()
)
config.addTargetAddr(
    snmpEngine, 'my-router',
    udp.domainName, ('195.218.195.228', 161),
    'my-creds'
)

@asyncio.coroutine
def snmpOperation(snmpEngine, target, contextEngineId, contextName,
                  nonRepeaters, maxRepetitions, varBinds):
    initialVarBinds = varBinds
    while varBinds:
        ( snmpEngine,
          errorIndication,
          errorStatus,
          errorIndex, 
          varBindTable ) = yield from cmdgen.BulkCommandGenerator().sendVarBinds(
            snmpEngine,
            target,
            contextEngineId,
            contextName,
            nonRepeaters,
            maxRepetitions,
            varBinds
          )

        if errorIndication:
            print(errorIndication)
            break
        elif errorStatus:
            print('%s at %s' % (
                    errorStatus.prettyPrint(),
                    errorIndex and varBindTable[-1][int(errorIndex)-1][0] or '?'
                )
            )
            break
        else:
            for varBindRow in varBindTable:
                for oid, val in varBindRow:
                    print('%s = %s' % (oid.prettyPrint(), val.prettyPrint()))

            errorIndication, varBinds = cmdgen.getNextVarBinds(
                initialVarBinds, varBindRow
            )
             
    # This also terminates internal timer
    config.delTransport(
        snmpEngine,
        udp.domainName
    ).closeTransport()

    loop.stop()

loop.run_until_complete(
    snmpOperation(
        snmpEngine,
        'my-router',
        None, '',  # contextEngineId, contextName 
        0, 25,     # nonRepeaters, maxRepetitions
        ( ('1.3.6.1.2.1.1', None), ('1.3.6.1.2.1.11', None) )
    )
)

loop.close()
